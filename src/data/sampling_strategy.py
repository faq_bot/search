import torch
import json
from typing import Any


class Top_k_sampling:
    """Top k sampling."""

    def __init__(
        self, message: str, tokenizer: Any, model: Any, top_k: int, data_path: str
    ) -> None:
        """Init.

        Args:
            message (str): user's message
            tokenizer (Any): huggingface tokenizer used
            model (Any): bert model used
            top_k (int): how many top-similar messages you want to appear
            data_path (str): path to data file with structure: {message[str]:its embedding[list]}
        """
        self.message = message
        self.tokenizer = tokenizer
        self.model = model
        self.top_k = top_k
        self.data_path = data_path

    def mean_pooling(
        self, model_output: torch.Tensor, attention_mask: torch.Tensor
    ) -> torch.Tensor:
        """Calculate mean pooling for given embeddings sequence.

        Args:
            model_output (torch.Tensor): output of the BERT-like model
            attention_mask (torch.Tensor): attention mask which indicates to the model which tokens it should be attended to

        Returns:
            torch.Tensor: result pooling - vectorize representation of the whole sequence
        """
        token_embeddings = model_output[
            0
        ]  # First element of model_output contains all token embeddings
        input_mask_expanded = (
            attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
        )
        sum_embeddings = torch.sum(token_embeddings * input_mask_expanded, 1)
        sum_mask = torch.clamp(input_mask_expanded.sum(1), min=1e-9)

        return sum_embeddings / sum_mask

    def vectorization(self) -> torch.Tensor:
        """Vectorize the input string using bert model and mean pooling.

        Returns:
            torch.Tensor: embedding representation of the whole sentence
        """
        encoded_input = self.tokenizer(
            self.message,
            padding=True,
            truncation=True,
            max_length=24,
            return_tensors="pt",
        )
        with torch.no_grad():
            model_output = self.model(**encoded_input)
        input_embeddings = self.mean_pooling(
            model_output, encoded_input["attention_mask"]
        )

        return input_embeddings[0]

    def forward(self):
        """Forward pass.

        Returns:
            list of sth
        """
        cos = torch.nn.CosineSimilarity(dim=0, eps=1e-6)
        embedded_answer = self.vectorization()
        bagger = {}
        f = open(self.data_path)
        data = json.load(f)
        f.close()
        for message in data:
            bagger[message] = round(
                float(cos(torch.tensor(data[message]), embedded_answer)), 4
            )

        ranking = sorted(list(bagger.items()), key=lambda x: x[1], reverse=True)[:5]
        output = [str(i + 1) + ": " + ranking[i][0] for i in range(len(ranking))]

        return output
