"""Tools to convert json with telegram messages to text."""

from typing import Union


def text_from_tg_message(msg: dict) -> Union[str, None]:
    """Obtain raw text from formatted telegram message.

    Args:
        msg: original message dict from tg chat json export

    Returns:
        every text entity concatenated to just one string separated with spaces

    # TODO links, mentions, hashtags, text formatting is excluded for now
    These below are text types already founded. The list is not exhaust.
    {'bank_card',
     'blockquote',
     'bold',
     'bot_command',
     'code',
     'custom_emoji',
     'email',
     'italic',
     'link',
     'mention',
     'mention_name',
     'phone',
     'pre',
     'spoiler',
     'strikethrough',
     'text_link',
     'underline'}
    """
    text_content = msg.get("text")
    if isinstance(text_content, list):
        _text = ""
        # Concatenate the text from all message items.
        for item in text_content:
            if isinstance(item, str):
                if item:
                    _text += " "
                    _text += item
            # FIXME Ignoring all messages with links for now.
            # Because of "Look what I found with AliExpress: [link]" messages.
            elif item.get("type") == "link":
                return ""
            elif item.get("type") == "hashtag":
                # Put hashtags without # mark
                if item.get("text"):
                    _text += " "
                    _text += item.get("text")[1:]
            elif item.get("text"):
                # There are mentions, links and other garbage types though.
                # TODO don't include _text of garbage types
                _text += " "
                _text += item.get("text")
        return _text
    if isinstance(msg.get("text"), str):
        return msg.get("text")
    return None
