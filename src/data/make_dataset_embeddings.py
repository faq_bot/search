import torch
import json
from typing import Any


class Chat_history:
    """Preprocessing telegram chats."""

    def __init__(self, tokenizer: Any, model: Any, json_data_path: str) -> None:
        """Init.

        Args:
            tokenizer (Any): huggingface tokenizer used
            model (Any): bert model used
            json_data_path (str): path to json with chat history
        """
        self.tokenizer = tokenizer
        self.model = model
        self.json_data_path = json_data_path

    def mean_pooling(
        self, model_output: torch.Tensor, attention_mask: torch.Tensor
    ) -> torch.Tensor:
        """Calculate mean pooling for given embeddings sequence.

        Args:
            model_output (torch.Tensor): output of the BERT-like model
            attention_mask (torch.Tensor): attention mask which indicates to the model which tokens it should be attended to

        Returns:
            torch.Tensor: result pooling - vectorize representation of the whole sequence
        """
        token_embeddings = model_output[
            0
        ]  # First element of model_output contains all token embeddings
        input_mask_expanded = (
            attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
        )
        sum_embeddings = torch.sum(token_embeddings * input_mask_expanded, 1)
        sum_mask = torch.clamp(input_mask_expanded.sum(1), min=1e-9)

        return sum_embeddings / sum_mask

    def create_dataset(self) -> None:
        """Generate data.json file with structure: {message[str]:its embedding[list]}."""
        bag = []
        f = open(self.json_data_path)
        data = json.load(f)
        f.close()
        for x in data["messages"]:
            if isinstance(x["text"], list):
                continue
            bag.append(x["text"])

        # take only first 1000 messages
        bag_first_1000 = bag[:1000]
        encoded_input = self.tokenizer(
            bag_first_1000,
            padding=True,
            truncation=True,
            max_length=24,
            return_tensors="pt",
        )
        with torch.no_grad():
            model_output = self.model(**encoded_input)

        embeddings = self.mean_pooling(model_output, encoded_input["attention_mask"])
        data_dict = {
            bag_first_1000[i]: embeddings[i, :].tolist()
            for i in range(len(bag_first_1000))
        }

        with open("data.json", "w") as file:
            json.dump(data_dict, file)

        return
