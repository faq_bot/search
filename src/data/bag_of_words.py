import re
import numpy as np
from tqdm.contrib.concurrent import process_map
from collections import Counter
import nltk
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
import string
from typing import Union, List
from src.data.tg_tools import text_from_tg_message

nltk.download("punkt")
nltk.download("stopwords")


def bow(text: Union[str, None]) -> Counter:
    """Process text to bag of words.

    Args:
        text:
            input string (message from the chat)

    Returns:
        counter object with count for text
    """
    if not text:
        return Counter()
    # Tokenization
    _tokens = word_tokenize(text)
    # Exclude punctuation
    pattern = f"[{re.escape(string.punctuation)}]"
    tokens = [re.sub(pattern, "", token) for token in _tokens]
    # Make lower
    tokens = [token.lower() for token in tokens if token]
    # Exclude stop tokens
    stop_words = set(stopwords.words("russian"))
    tokens = [token for token in tokens if token not in stop_words]

    # Exclude messages shorter than 10 meaningful words
    if len(tokens) <= 10:
        return Counter()

    # Stemming
    stemmer = SnowballStemmer("russian")
    tokens = [stemmer.stem(token) for token in tokens]

    return Counter(tokens)


def weighted_jaccard_similarity(counter1, counter2):
    """Compute weighted jaccard similarity.

    Args:
        counter1: Counter object representing the first bag of words
        counter2: Counter object representing the second bag of words

    Returns:
        weighted jaccard similarity
    """
    # Calculate the intersection and union, taking into account the counts of words
    intersection = sum(
        min(counter1.get(word, 0), counter2.get(word, 0))
        for word in set(counter1).union(set(counter2))
    )
    union = sum(
        max(counter1.get(word, 0), counter2.get(word, 0))
        for word in set(counter1).union(set(counter2))
    )
    return intersection / union if union else 0.0


def top_k_similar(msgs, index, k):
    """Find the top K messages with the highest Weighted Jaccard Similarity to a specified message.

    Args:
        msgs: List of messages, each message is a dictionary with a 'bow' key for the bag of words counter.
        index: Index of the message in msgs to compare against.
        k: Number of top similar messages to return.

    Returns:
        List of top K messages with the highest Weighted Jaccard Similarity.
    """
    # Get the counter for the specified message
    reference_counter = msgs[index].get("bow", Counter())
    print(msgs[index].get("text"))

    # Calculate the weighted Jaccard similarity for each message
    similarities = [
        (msg, weighted_jaccard_similarity(reference_counter, msg.get("bow", Counter())))
        for msg in msgs
        if msg != msgs[index]
    ]

    # Sort the messages based on their similarity
    similarities.sort(key=lambda x: x[1], reverse=True)

    # Return the top K messages
    return [(msg.get("text"), similarity) for msg, similarity in similarities[:k]]


def similarity_worker(args):
    """Just a wrapper to Jaccard sim but with indices.

    Args:
        args:

    """
    i, j, bow_i, bow_j = args
    similarity = weighted_jaccard_similarity(bow_i, bow_j)
    return i, j, similarity


def compute_similarity_matrix(bows: List[Counter], num_processes=10, size=1000):
    """Compute pairwise Weighted Jaccard Similarities between bags of words.

    Args:
        size:
        bows:
        num_processes:

    Returns:
        numpy matrix with dim size, size
    """
    # size = len(bows)
    similarity_matrix = np.zeros((size, size))
    args = [(i, j, bows[i], bows[j]) for i in range(size) for j in range(i + 1, size)]
    # Use process_map from tqdm.contrib.concurrent to display progress
    results = process_map(
        similarity_worker, args, max_workers=num_processes, chunksize=1000
    )

    # Update the similarity matrix with the results
    for i, j, similarity in results:
        similarity_matrix[i][j] = similarity_matrix[j][i] = similarity
    # Fill the diagonal with 1.0, as each message is identical to itself
    np.fill_diagonal(similarity_matrix, 1.0)

    return similarity_matrix


def msg_to_bow(msg: dict) -> Union[dict, None]:
    """Do whole pipeline of processing, cleaning, filtering.

    Args:
        msg: original message from telegram chat

    Returns:
        None if msg does not fulfill the requirements to be present in dataset (e.g. too short)
        msg with added Bag Of Words dict (key 'bow') if successfully cleaned
    """
    text = text_from_tg_message(msg)
    if bow(text):
        cleaned_msg = msg
        cleaned_msg["bow"] = bow(text)
        return cleaned_msg
    return None
