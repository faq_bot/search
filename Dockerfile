FROM mambaorg/micromamba
WORKDIR /app

COPY environment.yml .
COPY pyproject.toml .
COPY --chown=$MAMBA_USER:$MAMBA_USER environment.yml /tmp/env.yaml
RUN micromamba install -y -n base -f /tmp/env.yaml && \
    micromamba clean --all --yes
ARG MAMBA_DOCKERFILE_ACTIVATE=1
RUN pip install poetry
# Install pypi dependencies with poetry
RUN poetry config virtualenvs.create false  \
    && poetry install --no-root

COPY . .
