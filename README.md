search
==============================
Find answers to frequently asked questions

## Content
- [Setting up the environment](#setting-up-the-environment)
- [Methodology](#methodology)
- [Data Version Control (DVC)](#data-version-control-dvc)
- [Project Structure](#project-organization)

## Setting up a local environment
Prerequisites: conda/mamba installed. It's recommended to start with [Mamba](https://mamba.readthedocs.io/en/latest/installation/mamba-installation.html) and [Miniforge](https://github.com/conda-forge/miniforge) distribution.
* Create a conda environment with all "scientific" packages (packages with non-python dependencies, i.e. torch).
```
conda env create --file=environment.yml -n=search
```
* Now activate the environment.
```
conda activate search
```

* To use poetry with pure python packages install it:
```
conda install poetry
```
* Since project use conda environment for scientific packages (== with non-python dependencies),
 make sure poetry recognises conda/mamba environment folder and reuses your conda/mamba environment.
```
poetry config virtualenvs.create false
```
* Make sure the outputs for python path are the same:
```
which python3
```
```
poetry run which python3
```
* Install (pure python) dependencies from pyproject.toml file by passing:
```
poetry install
```
If you need more information about interaction with poetry you can check basic usage [guide](https://python-poetry.org/docs/basic-usage/)

## Docker
1. Make sure docker is installed
2. Run the development docker image with
   ```
   docker compose up
   ```
3. If you need interactive shell, use:
   ```
   docker compose exec -it search bash
   ```
4. Alternatively, with just docker:
   ```
   docker build -t search .
   ```
   ```
   docker run -it --name search --rm -v $(pwd)/data:/app/data -v $(pwd)/src:/app/src -v $(pwd)/notebooks:/app/notebooks -p 8888:8888 search bash
   ```
## Methodology
There are two repos. First is DS repository with github flow:
<img src="https://user-images.githubusercontent.com/6351798/48032310-63842400-e114-11e8-8db0-06dc0504dcb5.png">

Second repo is to develop a production service, the methodology is similar to gitflow. We decide to simplify the workflow's tree to three type of branches: main, develop and feature, so it looks like this:

We have feature branches that use develop as a parent branch. When feature is complete we merge it back into the develop branch. Note that we do not have release branch and use develop branch for tests as well. After tests have been passed in develop branch, it is merged into the main and tagged with a version number.
<img src="https://wac-cdn.atlassian.com/dam/jcr:34c86360-8dea-4be4-92f7-6597d4d5bfae/02%20Feature%20branches.svg?cdnVersion=1539" width=500>

## ClearML
ClearML is used to track experiments and log artifacts. Check clearml.ipynb and model.ipynb for examples.

## Data Version Control (DVC)
At the moment we use DVC to track some data stored in the common gdrive:
https://dvc.org/doc/user-guide/data-management/remote-storage/google-drive#configuration-parameters

So we update the data via dvc and only meta-info of that data (with .dvc extension) with git.
So any time you update the data itself use 2-steps process to push:
1. Update the data with DVC
   1. `$dvc add data/raw/SomeData`
   2. `$dvc push`
2. Update the meta-info with git
   1. `$git add data/raw/SomeData.dvc`
   2. `$git commit `
   3. `$git push`

One may use `$dvc config core.autostage true` to dvc files staged (`$git add .../.dvc`) automatically
https://dvc.org/doc/user-guide/data-management/remote-storage/google-drive#configuration-parameters
Check also `.dvc/config` for some extra params

## Project Organization
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A Quarto project;
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py



--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
